Router.configure({
    layoutTemplate: 'AppLayout'
});

Router.route('/', function () {
  this.render('Home');
});

Router.route('/about', function () {
  this.render('About');
});

Router.route('/menu', function () {
  this.render('Menu');
});

Router.route('/booking', function () {
  this.render('Booking');
});

Router.route('/howtos', function () {
  this.render('HowTos');
});